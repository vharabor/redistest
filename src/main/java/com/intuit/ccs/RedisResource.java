package com.intuit.ccs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/redis")
public class RedisResource {
	
	@Autowired 
	UserRepository userRepository;
	
	
	
	@RequestMapping(value = "/set", method = RequestMethod.GET)
	public void set(){
		
		 User userA = new User();
		    userA.setUsername("userA");
		    userRepository.save(userA);

		    User userB = new User();
		    userB.setUsername("userB");
		    userRepository.save(userB);
		
	}
	@RequestMapping(value = "/get", method = RequestMethod.GET)
	public void getValue(){
		
		 	//Use the redis monitor command to view the commands sent to redis
			//they will both be the same
		    userRepository.findByUsername("userA");
		    userRepository.findByUsername("userB");
		    
		
	}
	

}

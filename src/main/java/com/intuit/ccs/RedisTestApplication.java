package com.intuit.ccs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

@SpringBootApplication
public class RedisTestApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(RedisTestApplication.class, args);
	}
}
